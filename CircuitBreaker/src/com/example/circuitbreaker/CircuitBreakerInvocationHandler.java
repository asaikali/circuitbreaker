/* 
 * Copyright 2009 Programming Mastery Inc.  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not 
 * use this file except in compliance with the License. You may obtain a copy 
 * of the License at 
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0 
 *   
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations 
 * under the License.
 * 
 */
package com.example.circuitbreaker;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


class CircuitBreakerInvocationHandler implements InvocationHandler
{
	final private CircuitBreaker breaker; 
	final private Object instance; 
	
	public CircuitBreakerInvocationHandler(Object instance, String domain, String name)
	{
		this.breaker = new CircuitBreaker(domain,name);
		this.instance = instance; 
	}
	
	@Override
	public Object invoke(Object proxy, final Method method, final Object[] args) throws Throwable {
	
		Object result; 
		result = this.breaker.execute( new CircuitBreakerCallback(){

			@Override
			public Object doInBreaker(){
					try {
						return method.invoke(CircuitBreakerInvocationHandler.this.instance, args);	
					} catch (IllegalAccessException e) {
						// All calls comming through this invocationdaler came on a public 
						// method of an interface so we should not get this exception ever. 	
						throw new IllegalStateException("Should not have gotten an IllegalAccessException",e);
					} catch (InvocationTargetException e) {
						if( e.getCause() instanceof RuntimeException){
							throw (RuntimeException)e.getCause();
						}
					}
					
					return null;
			}
			
		});
		
		return result;
	}
	
}