/* 
 * Copyright 2009 Programming Mastery Inc.  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not 
 * use this file except in compliance with the License. You may obtain a copy 
 * of the License at 
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0 
 *   
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations 
 * under the License.
 * 
 */

package com.example.circuitbreaker;

import java.lang.management.ManagementFactory;
import java.lang.reflect.Proxy;

import javax.management.JMException;
import javax.management.MBeanServer;
import javax.management.ObjectName;

public class CircuitBreaker implements CircuitBreakerMXBean {
	private final ObjectName objectName; 
	private final CircuitBreakerConfiguration configuration; 
	private final CircuitBreakerState state;
	
	public CircuitBreaker(String domain, String name)
	{
		try {
			this.configuration = new CircuitBreakerConfiguration();
			this.state = new CircuitBreakerState(); 
			this.objectName = new ObjectName(domain + ":" + "type=CircuitBreaker,name=" + name);
			MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
			mbs.registerMBean(this,objectName); 
		} catch (JMException e) {
			throw new RuntimeException(e);
		} 
	}

	@SuppressWarnings("unchecked")
	public static <T> T protect(Object instance, Class<T> interfaceToProtect, String domain, String name){
		
		// setup the interfaces parameter to the proxy 
		if( interfaceToProtect.isInterface() == false){
				throw new IllegalArgumentException("interafaceToProtect must be an interface");
		}
		
		@SuppressWarnings("rawtypes")
		Class[] interfaces = new Class[1];
		interfaces[0] = interfaceToProtect; 
		
		// create an instance of the handler 
		CircuitBreakerInvocationHandler handler = new CircuitBreakerInvocationHandler(instance, domain, name);
		
		// create a proxy screen 
		T proxy = (T) Proxy.newProxyInstance(interfaceToProtect.getClassLoader(), interfaces, handler);
		
		return proxy; 
	}

	public Object execute(CircuitBreakerCallback callback)
	{
		System.out.println("CircuitBreaker.execute()");
		CircuitBreakerStatus status = this.state.getCurrentState(this.configuration);
		System.out.println("CircuitBreaker state is: " + status);
		
		if(status == CircuitBreakerStatus.open){
			this.state.updateCumulativeCallsWhenOpen();
			throw new CircuitBreakerOpenException(this.objectName.getCanonicalName());
		} 
		else if( status == CircuitBreakerStatus.halfOpen){
			try {
				Object result = callback.doInBreaker();
				this.state.close();
				return result; 
			}
			catch(RuntimeException e) { 
				this.state.open(); 
				throw e;
			} catch(Error e){
				this.state.open();
				throw e;
			}
				
		} 
		else if ( status == CircuitBreakerStatus.closed){
			try {	
				return callback.doInBreaker(); 
			} catch(RuntimeException e) {
				this.state.recordFailure(this.configuration); 
				throw e;
			} catch(Error e){
				this.state.recordFailure(this.configuration);
				throw e; 
			}
		}
		
		return null;
	}

	@Override
	public void close() {
		System.out.println("CircuitBreaker.close()");
		this.state.open();
	}

	@Override
	public int getCumulativeFailureCount() {
		System.out.println("CircuitBreaker.getCumulativeFailureCount()");
		return this.state.getCumulativeFailureCount();
	}

	@Override
	public long getResetTimeInMilliSeconds() {
		System.out.println("CircuitBreaker.getDurationBeforeHalfOpenInMilliSeconds()");
		return this.configuration.getResetTimeMilliSeconds();
	}

	@Override
	public int getCurrentFailureCount() {
		System.out.println("CircuitBreaker.getFailureCount()");
		return this.state.getCurrentFailureCount();
	}

	@Override
	public int getMaxFailuresBeforeOpening() {
		System.out.println("CircuitBreaker.getMaxFailuresBeforeOpening()");
		return this.configuration.getMaxFailuresBeforeOpening();
	}

	@Override
	public CircuitBreakerStatus getStatus() {
		System.out.println("CircuitBreaker.getStatus()");
		return this.state.getStatus();
	}

	@Override
	public void halfOpen() {
		System.out.println("CircuitBreaker.halfOpen()");
		this.state.halfOpen();	
	}

	@Override
	public void open() {
		System.out.println("CircuitBreaker.open()");
		this.state.open();
	}

	@Override
	public void setResetTimeInMilliSeconds(long duration) {
		System.out.println("CircuitBreaker.setDurationBeforeHalfOpenInMilliSeconds()");
		this.configuration.setResetTimeMilliSeconds(duration);
	}

	@Override
	public void setMaxFailuresBeforeOpening(int maxFailuresBeforeOpening) {
		System.out.println("CircuitBreaker.setMaxFailuresBeforeOpening()");
		this.configuration.setMaxFailuresBeforeOpening(maxFailuresBeforeOpening);
	}

	@Override
	public int getCumultaviteCallsWhenOpen() {
		return this.state.getCummulativeCallsWhenOpen();
	}
}
