/* 
 * Copyright 2009 Programming Mastery Inc.  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not 
 * use this file except in compliance with the License. You may obtain a copy 
 * of the License at 
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0 
 *   
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations 
 * under the License.
 * 
 */
package com.example.circuitbreaker;

class CircuitBreakerConfiguration {
	private int maxFailuresBeforeOpening;
	private long resetTimeMilliSeconds; 
	
	public CircuitBreakerConfiguration()
	{
		synchronized (this) {
			this.maxFailuresBeforeOpening = 2; 
			this.resetTimeMilliSeconds = 60000;
		}
	}
	
	public CircuitBreakerConfiguration(int maxFailuresBeforeOpening, long durationBeforeHalfOpenInMilliSeconds)
	{
		if(maxFailuresBeforeOpening < 1) throw new IllegalArgumentException("maxFailuresBeforeOpening must be > 0");
		if(durationBeforeHalfOpenInMilliSeconds < 0) throw new IllegalArgumentException(" durationBeforeHalfOpenInMilliSeconds must be > 0");
		synchronized (this) {
			this.maxFailuresBeforeOpening = maxFailuresBeforeOpening;
			this.resetTimeMilliSeconds = durationBeforeHalfOpenInMilliSeconds;	
		}
		
	}

	synchronized public int getMaxFailuresBeforeOpening() {
		return maxFailuresBeforeOpening;
	}

	synchronized public long getResetTimeMilliSeconds() {
		return resetTimeMilliSeconds;
	}

	synchronized public void setMaxFailuresBeforeOpening(int maxFailuresBeforeOpening) {
		this.maxFailuresBeforeOpening = maxFailuresBeforeOpening;
	}

	synchronized public void setResetTimeMilliSeconds(long resetTimeMilliSeconds) {
		this.resetTimeMilliSeconds = resetTimeMilliSeconds;
	}
}
