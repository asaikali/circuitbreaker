/* 
 * Copyright 2009 Programming Mastery Inc.  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not 
 * use this file except in compliance with the License. You may obtain a copy 
 * of the License at 
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0 
 *   
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations 
 * under the License.
 * 
 */
package com.example.circuitbreaker;

class CircuitBreakerState {
	private int currentFailureCount = 0;
	private int cumulativeFailureCount = 0; 
	private CircuitBreakerStatus status = CircuitBreakerStatus.closed;
	private long openTimestamp = 0;
	private int cummulativeCallsWhenOpen = 0; 
	
	synchronized public int getCurrentFailureCount() {
		return currentFailureCount;
	}
	
	synchronized public int getCumulativeFailureCount() {
		return cumulativeFailureCount;
	}
	
	synchronized public CircuitBreakerStatus getStatus() {
		return status;
	}
	
	synchronized public void  close() {
		this.status = CircuitBreakerStatus.closed;
		this.currentFailureCount = 0;
	}

	synchronized public void halfOpen() {
		this.status = CircuitBreakerStatus.halfOpen; 
	}

	synchronized public void open() {		
		this.status = CircuitBreakerStatus.open; 
		this.openTimestamp = System.currentTimeMillis();
	}
	
	synchronized void recordFailure(CircuitBreakerConfiguration config)
	{
		this.currentFailureCount++;
		this.cumulativeFailureCount++;
		
		if( this.currentFailureCount > config.getMaxFailuresBeforeOpening()) this.open();
	}
	
	synchronized public CircuitBreakerStatus getCurrentState(CircuitBreakerConfiguration config) {
		
		if( this.status == CircuitBreakerStatus.open){
			long now = System.currentTimeMillis(); 
			long duration = now - this.openTimestamp; 
			
			if( duration > config.getResetTimeMilliSeconds())
			{
				this.status = CircuitBreakerStatus.halfOpen; 
				this.openTimestamp = System.currentTimeMillis();
			}	
		}
		
		return this.status;
	}
	
	synchronized void updateCumulativeCallsWhenOpen()
	{
		this.cummulativeCallsWhenOpen++;
	}

	synchronized public int getCummulativeCallsWhenOpen() {
		return cummulativeCallsWhenOpen;
	}
}
