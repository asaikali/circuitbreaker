/* 
 * Copyright 2009 Programming Mastery Inc.  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not 
 * use this file except in compliance with the License. You may obtain a copy 
 * of the License at 
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0 
 *   
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations 
 * under the License.
 * 
 */

package com.example.circuitbreaker.test.remotesystem;

public class RemoteSystem implements IRemoteSystem {
	boolean failOnNextCall = false; 
	

	public void setFailOnNextCall(boolean failOnNextCall) {
		this.failOnNextCall = failOnNextCall;
	}

	public void bar()
	{
		if(this.failOnNextCall)
		{	
			throw new RemoteSystemException("bar operation failed");
		}
		
		System.out.println("barOperation worked!");
	}
}
