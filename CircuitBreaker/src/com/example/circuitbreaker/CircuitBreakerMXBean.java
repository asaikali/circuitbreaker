/* 
 * Copyright 2009 Programming Mastery Inc.  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not 
 * use this file except in compliance with the License. You may obtain a copy 
 * of the License at 
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0 
 *   
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations 
 * under the License.
 * 
 */
package com.example.circuitbreaker;

public interface CircuitBreakerMXBean 
{	
	// max failures before going into open state 
	public int getMaxFailuresBeforeOpening();
	public void setMaxFailuresBeforeOpening(int maxFailuresBeforeOpening); 
	//public long getFailureIntervalInMilliSeconds(); 
	//public void setFailureIntervalInMilliSeconds(); 
	
	// duration before going into half open state 
	public long getResetTimeInMilliSeconds();
	public void setResetTimeInMilliSeconds(long duration);
	
	// get failure counts 
	public int getCurrentFailureCount();
	public int getCumulativeFailureCount();
	public int getCumultaviteCallsWhenOpen();
	
	// state related functions 
	public CircuitBreakerStatus getStatus();
	public void open();
	public void close();
	public void halfOpen();
}